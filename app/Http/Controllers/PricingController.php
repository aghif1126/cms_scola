<?php

namespace App\Http\Controllers;

use App\Models\pricing;
use App\Models\privilage;
use Illuminate\Http\Request;

class PricingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function pricingTable(){
        $pricings = Pricing::all();
        // dd($pricings);
        return view("admin.pricing.pricingTable", compact("pricings"));
    }
    public function addPricing(){
        $pricing = null;
        return view('admin.pricing.addPricing', compact('pricing'));
    }
    public function editPricing(Request $request){
        // dd($request);
        $pricing = Pricing::find($request -> id);
        return view('admin.pricing.addPricing', compact('pricing'));
    }
    public function storePricing(Request $request){
            // dd($request);
            Pricing::insert([
                'categori'=>$request->categori,
                'price'=>$request->price,
                'subdesc'=>$request->subdesc,
                'star'=>$request->star,
            ]);
            $max_id = Pricing::max('id');
            $privilage = $request -> privilage;
            foreach ($privilage as $item) {
                Privilage::insert([
                    'pricing_id' => $max_id,
                    'privilage' => $item
                ]);
            }


        return redirect(route('admin.pricing-table'));
    }

    public function updatePricing(Request $request){
        // dd($request);
        $pricing = Pricing::find($request -> id);

        $pricing->update([
            'categori'=>$request->categori,
            'price'=>$request->price,
            'subdesc'=>$request->subdesc,
            'star'=>$request->star,
        ]);
        $max_id = $pricing -> id;
        Privilage::where('pricing_id', $pricing -> id) -> delete();
        $privilage = $request -> privilage;
        foreach ($privilage as $item) {

           if($item){
            Privilage::insert([
                'pricing_id' => $max_id,
                'privilage' => $item
            ]);
           }
        }



        return redirect(route('admin.pricing-table'));
    }

    public function destroyPricing(Request $request){
        // dd($request);
        $pricing = Pricing::find($request -> id);
        Privilage::where('pricing_id', $pricing -> id) -> delete();
        $pricing -> delete();
        return back();
    }


}
