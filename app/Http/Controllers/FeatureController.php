<?php

namespace App\Http\Controllers;

use App\Models\feature;
use Illuminate\Http\Request;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function featureTable(){
        $features = Feature::all();
        // dd($features);
        return view("admin.feature.featureTable", compact("features"));
    }
    public function addFeature(){
        $feature = null;
        return view('admin.feature.addFeature', compact('feature'));
    }
    public function editFeature(Request $request){
        // dd($request);
        $feature = Feature::find($request -> id);
        return view('admin.feature.addFeature', compact('feature'));
    }
    public function storeFeature(Request $request){
            // dd($request);
            Feature::insert([
                'title'=>$request->title,
                'desc'=>$request->description,
                'subdesc'=>$request->subdescription
            ]);


        return redirect(route('admin.feature-table'));
    }

    public function updateFeature(Request $request){
        // dd($request);
        $feature = Feature::find($request -> id);

            $feature -> update([
                'title'=>$request->title,
                'desc'=>$request->description,
                'subdesc'=>$request->subdescription
            ]);


        return redirect(route('admin.feature-table'));
    }

    public function destroyFeature(Request $request){
        // dd($request);
        $feature = Feature::find($request -> id);
        $feature -> delete();
        return back();
    }


}
