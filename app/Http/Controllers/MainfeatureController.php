<?php

namespace App\Http\Controllers;

use App\Models\mainfeature;
use Illuminate\Http\Request;

class MainfeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function mainfeatureTable(){
        $mainfeatures = MainFeature::all();
        // dd($mainfeatures);
        return view("admin.mainfeature.mainfeatureTable", compact("mainfeatures"));
    }
    public function addMainFeature(){
        $mainfeature = null;
        return view('admin.mainfeature.addMainFeature', compact('mainfeature'));
    }
    public function editMainFeature(Request $request){
        // dd($request);
        $mainfeature = MainFeature::find($request -> id);
        return view('admin.mainfeature.addMainFeature', compact('mainfeature'));
    }
    public function storeMainFeature(Request $request){
            // dd($request);
            MainFeature::insert([
                'title'=>$request->title,
                'href' => $request -> href

            ]);


        return redirect(route('admin.mainfeature-table'));
    }

    public function updateMainFeature(Request $request){
        // dd($request);
        $mainfeature = MainFeature::find($request -> id);

            $mainfeature -> update([
                'title'=>$request->title,
                'href' => $request -> href

            ]);


        return redirect(route('admin.mainfeature-table'));
    }

    public function destroyMainFeature(Request $request){
        // dd($request);
        $mainfeature = MainFeature::find($request -> id);
        $mainfeature -> delete();
        return back();
    }


}
