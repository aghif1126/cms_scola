<?php

namespace App\Http\Controllers;

use App\Models\quote;
use Illuminate\Http\Request;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function quoteTable(){
        $quotes = Quote::all();
        // dd($quotes);
        return view("admin.quote.quoteTable", compact("quotes"));
    }
    public function addQuote(){
        $quote = null;
        return view('admin.quote.addQuote', compact('quote'));
    }
    public function editQuote(Request $request){
        // dd($request);
        $quote = Quote::find($request -> id);
        return view('admin.quote.addQuote', compact('quote'));
    }
    public function storeQuote(Request $request){
            // dd($request);
            Quote::insert([
                'title'=>$request->title,

            ]);


        return redirect(route('admin.quote-table'));
    }

    public function updateQuote(Request $request){
        // dd($request);
        $quote = Quote::find($request -> id);

            $quote -> update([
                'title'=>$request->title,

            ]);


        return redirect(route('admin.quote-table'));
    }

    public function destroyQuote(Request $request){
        // dd($request);
        $quote = Quote::find($request -> id);
        $quote -> delete();
        return back();
    }


}
