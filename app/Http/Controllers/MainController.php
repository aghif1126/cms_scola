<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Models\feature;
use App\Models\mainfeature;
use App\Models\pricing;
use App\Models\quote;
use Illuminate\Http\Request;

class MainController extends Controller
{

    public function index() {
        $contents = Content::all();
        $features = feature::all();
        $mainfeatures = mainfeature::all();
        $pricings = pricing::all();
        $quotes = quote::all();
        return view('index', compact('contents', 'features', 'mainfeatures', 'pricings', 'quotes'));


    }
}
