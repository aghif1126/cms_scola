<?php

namespace App\Http\Controllers;

use App\Models\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function contentTable(){
        $contents = Content::all();
        // dd($contents);
        return view("admin.content.contentTable", compact("contents"));
    }
    public function addContent(){
        $content = null;
        return view('admin.content.addContent', compact('content'));
    }
    public function editContent(Request $request){
        // dd($request);
        $content = Content::find($request -> id);
        return view('admin.content.addContent', compact('content'));
    }
    public function storeContent(Request $request){
        // dd($request->image);
       if ($image = $request->file('image')) {
            $destinationPath = 'template/img/';
            $profileImage = "image" .date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
            Content::insert([
                'title'=>$request->title,
                'desc'=>$request->description,
                'image' => $profileImage
            ]);

        }else{
            Content::insert([
                'title'=>$request->title,
                'desc'=>$request->description
            ]);
        }

        return redirect(route('admin.content-table'));
    }

    public function updateContent(Request $request){
        // dd($request);
        $content = Content::find($request -> id);
       if ($image = $request->file('image')) {
            $destinationPath = 'template/img/';
            $profileImage = "image" .date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";

            $content -> update([
                'title'=>$request->title,
                'desc'=>$request->description,
                'image' => $profileImage
            ]);

        }else{
            $content -> update([
                'title'=>$request->title,
                'desc'=>$request->description
            ]);
        }

        return redirect(route('admin.content-table'));
    }

    public function destroyContent(Request $request){
        // dd($request);
        $content = Content::find($request -> id);
        $content -> delete();
        return back();
    }


}
