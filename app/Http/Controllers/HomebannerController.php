<?php

namespace App\Http\Controllers;

use App\Models\homebanner;
use Illuminate\Http\Request;

class HomebannerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(homebanner $homebanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(homebanner $homebanner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, homebanner $homebanner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(homebanner $homebanner)
    {
        //
    }
}
