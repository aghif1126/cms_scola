<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pricing extends Model
{
    use HasFactory;
    protected $guarded = [''];



    /**
     * Get all of the privilage for the pricing
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function privilage()
    {
        return $this->hasMany(privilage::class, 'pricing_id', 'id');
    }
}
