<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\FeatureController;
use App\Http\Controllers\MainfeatureController;
use App\Http\Controllers\PricingController;
use App\Http\Controllers\QuoteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });
Route::get('/home', function () {
    return redirect(route('admin.home'));
});

Route::get('login', [LoginController::class, 'form'])->name('login')->middleware('guest');
Route::post('authenticate', [LoginController::class, 'authenticate']);
Route::get('logout', [LoginController::class, 'logout'])->middleware('auth')->name('logout');


Route::get('/', [MainController::class, 'index'])->name('login')->middleware('guest');


Route::middleware('auth')->group(function () {
    Route::name('admin.')->group(function () {
        Route::get('dashboard', [AdminController::class, 'index'])->name('home');

        Route::get('content-table', [ContentController::class, 'contentTable'])->name('content-table');
        Route::get('add-content', [ContentController::class, 'addContent'])->name('add-content');
        Route::post('store-content', [ContentController::class, 'storeContent'])->name('store-content');
        Route::post('edit-content', [ContentController::class, 'editContent'])->name('edit-content');
        Route::post('update-content', [ContentController::class, 'updateContent'])->name('update-content');
        Route::post('destroy-content', [ContentController::class, 'destroyContent'])->name('destroy-content');

        Route::get('feature-table', [FeatureController::class, 'featureTable'])->name('feature-table');
        Route::get('add-feature', [FeatureController::class, 'addFeature'])->name('add-feature');
        Route::post('store-feature', [FeatureController::class, 'storeFeature'])->name('store-feature');
        Route::post('edit-feature', [FeatureController::class, 'editFeature'])->name('edit-feature');
        Route::post('update-feature', [FeatureController::class, 'updateFeature'])->name('update-feature');
        Route::post('destroy-feature', [FeatureController::class, 'destroyFeature'])->name('destroy-feature');

        Route::get('mainfeature-table', [MainfeatureController::class, 'mainfeatureTable'])->name('mainfeature-table');
        Route::get('add-mainfeature', [MainfeatureController::class, 'addMainFeature'])->name('add-mainfeature');
        Route::post('store-mainfeature', [MainfeatureController::class, 'storeMainFeature'])->name('store-mainfeature');
        Route::post('edit-mainfeature', [MainfeatureController::class, 'editMainFeature'])->name('edit-mainfeature');
        Route::post('update-mainfeature', [MainfeatureController::class, 'updateMainFeature'])->name('update-mainfeature');
        Route::post('destroy-mainfeature', [MainfeatureController::class, 'destroyMainFeature'])->name('destroy-mainfeature');

        Route::get('quote-table', [QuoteController::class, 'quoteTable'])->name('quote-table');
        Route::get('add-quote', [QuoteController::class, 'addQuote'])->name('add-quote');
        Route::post('store-quote', [QuoteController::class, 'storeQuote'])->name('store-quote');
        Route::post('edit-quote', [QuoteController::class, 'editQuote'])->name('edit-quote');
        Route::post('update-quote', [QuoteController::class, 'updateQuote'])->name('update-quote');
        Route::post('destroy-quote', [QuoteController::class, 'destroyQuote'])->name('destroy-quote');

        Route::get('pricing-table', [PricingController::class, 'pricingTable'])->name('pricing-table');
        Route::get('add-pricing', [PricingController::class, 'addPricing'])->name('add-pricing');
        Route::post('store-pricing', [PricingController::class, 'storePricing'])->name('store-pricing');
        Route::post('edit-pricing', [PricingController::class, 'editPricing'])->name('edit-pricing');
        Route::post('update-pricing', [PricingController::class, 'updatePricing'])->name('update-pricing');
        Route::post('destroy-pricing', [PricingController::class, 'destroyPricing'])->name('destroy-pricing');


    });

});



