@extends('layout.admin')
@section('content')

<div class="card">
    <form class="row g-3" action="{{$feature ?  route('admin.update-feature') : route('admin.store-feature')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @if ($feature)
            <input type="text" name="id" value="{{ $feature -> id }}" hidden>
        @endif
    <div class="card-body">



        <!-- Floating Labels Form -->
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="title" value="{{ $feature ? $feature -> title : ''}}" placeholder="Your Name">
                    <label for="floatingName">Title</label>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="form-floating">
                    <textarea class="form-control" placeholder="Address" name="description" id="floatingTextarea"
                    style="height: 100px;">{{ $feature ? $feature -> desc : ''}}</textarea>
                    <label for="floatingTextarea">Description</label>
                </div>
            </div>

            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="subdescription" value="{{ $feature ? $feature -> subdesc : ''}}" placeholder="Your Name">
                    <label for="floatingName">Subdesc</label>
                </div>
            </div>





    <div class="text-center mt-5">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
    </div>
    </form><!-- End floating Labels Form -->

    <!-- End Quill Editor Default -->

</div>




@endsection
