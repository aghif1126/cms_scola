@extends('layout.admin')
@section('title', 'Content')
@section('breadcrumb', 'Content')
@section('content')

<div class="d-flex justify-content-end">
    <a href="{{ route('admin.add-content') }}">
        <button type="button" class="btn btn-primary me-3 mb-3">Add Content</button>
    </a>
</div>
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Content Table</h5>

        <!-- Default Table -->
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Image</th>
                    <th scope="col" style="width: 10%; text-align: center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contents as $var)
                <tr>
                    <th scope="row">{{ $loop->iteration  }}</th>
                    <td>{{ $var -> title }}</td>
                    <td>{{ $var -> desc }}</td>
                    <td>
                        <img src="{{ asset('template/img/'.$var -> image) }}" alt="" width="100px" height="100px">

                    </td>
                    <td>
                        <div class="d-flex justify-content-end">
                                <form action="{{ route('admin.edit-content')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name='id' value="{{ $var -> id }}">
                                <button type="submit" class="mx-3 btn btn-warning"><i class="bi bi-pencil-square"></i></button>
                            </form>
                            <form action="{{ route('admin.destroy-content')}}" method="POST">
                                @csrf
                                <input type="hidden" name='id' value="{{ $var -> id }}">
                                <button  type="submit" class="me-3 btn btn-danger"><i class="bi bi-trash"></i></button>
                            </form>

                            </td>
                        </div>
                    </td>


                  </tr>
                @endforeach
            </tbody>
        </table>
        <!-- End Default Table Example -->
    </div>
</div>
@endsection
