@extends('layout.admin')
@section('content')

<div class="card">
    <form class="row g-3" action="{{$content ?  route('admin.update-content') : route('admin.store-content')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @if ($content)
            <input type="text" name="id" value="{{ $content -> id }}" hidden>
        @endif
    <div class="card-body">



        <!-- Floating Labels Form -->
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="title" value="{{ $content ? $content -> title : ''}}" placeholder="Your Name">
                    <label for="floatingName">Title</label>
                </div>
            </div>
            {{-- @dd($content -> desc) --}}
            <div class="col-12 mb-3">
                <div class="form-floating">
                    <textarea class="form-control" placeholder="Address" name="description" id="floatingTextarea"
                        style="height: 100px;">{{ $content ? $content -> desc : ''}}</textarea>
                    <label for="floatingTextarea">Description</label>
                </div>
            </div>

            @if ($content)
                <img src="{{ asset('template/img/'.$content -> image) }}" alt="" width="150px" height="150px" class="my-3">
                <br>
                <br>
            @endif


            <div class="row mb-3">
                <label for="inputNumber" class="col-sm-2 col-form-label">{{ $content ? "Update Image" : "Add Image" }}</label>
                <div class="col-sm-10">
                  <input class="form-control" type="file" name="image" id="formFile">
                </div>
              </div>




    <div class="text-center mt-5">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
    </div>
    </form><!-- End floating Labels Form -->

    <!-- End Quill Editor Default -->

</div>




@endsection
