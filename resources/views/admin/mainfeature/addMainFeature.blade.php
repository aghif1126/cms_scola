@extends('layout.admin')
@section('content')

<div class="card">
    <form class="row g-3"
        action="{{$mainfeature ?  route('admin.update-mainfeature') : route('admin.store-mainfeature')}}" method="POST"
        enctype="multipart/form-data">
        @csrf
        @if ($mainfeature)
        <input type="text" name="id" value="{{ $mainfeature -> id }}" hidden>
        @endif
        <div class="card-body">



            <!-- Floating Labels Form -->
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="title"
                        value="{{ $mainfeature ? $mainfeature -> title : ''}}" placeholder="Your Name">
                    <label for="floatingName">Title</label>
                </div>
            </div>
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="href"
                        value="{{ $mainfeature ? $mainfeature -> href : ''}}" placeholder="Link">
                    <label for="floatingName">Link</label>
                </div>
            </div>
            <div class="col-12 mb-3">

                <div class="text-center mt-5">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
    </form><!-- End floating Labels Form -->

    <!-- End Quill Editor Default -->

</div>




@endsection
