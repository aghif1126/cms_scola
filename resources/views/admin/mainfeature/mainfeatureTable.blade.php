@extends('layout.admin')
@section('title', 'MainFeature')
@section('breadcrumb', 'MainFeature')
@section('content')

<div class="d-flex justify-content-end">
    <a href="{{ route('admin.add-mainfeature') }}">
        <button type="button" class="btn btn-primary me-3 mb-3">Add Feature</button>
    </a>
</div>
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Main Feature Table</h5>

        <!-- Default Table -->
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Href</th>
                    <th scope="col" style="width: 10%; text-align: center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($mainfeatures as $var)
                <tr>
                    <th scope="row">{{ $loop->iteration  }}</th>
                    <td>{{ $var -> title }}</td>
                    <td>{{ $var -> href }}</td>


                    <td>
                        <div class="d-flex justify-content-end">
                                <form action="{{ route('admin.edit-mainfeature')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name='id' value="{{ $var -> id }}">
                                <button type="submit" class="mx-3 btn btn-warning"><i class="bi bi-pencil-square"></i></button>
                            </form>
                            <form action="{{ route('admin.destroy-mainfeature')}}" method="POST">
                                @csrf
                                <input type="hidden" name='id' value="{{ $var -> id }}">
                                <button  type="submit" class="me-3 btn btn-danger"><i class="bi bi-trash"></i></button>
                            </form>

                            </td>
                        </div>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <!-- End Default Table Example -->
    </div>
</div>
@endsection
