@extends('layout.admin')
@section('title', 'Pricing')
@section('breadcrumb', 'Pricing')
@section('content')

<div class="d-flex justify-content-end">
    <a href="{{ route('admin.add-pricing') }}">
        <button type="button" class="btn btn-primary me-3 mb-3">Add Pricing</button>
    </a>
</div>
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Pricing Table</h5>

        <!-- Default Table -->
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Categori</th>
                    <th scope="col">Price</th>
                    <th scope="col">Subdesc</th>
                    <th scope="col">Star</th>
                    <th scope="col">Privilage</th>
                    <th scope="col" style="width: 10%; text-align: center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pricings as $var)
                <tr>
                    <th scope="row">{{ $loop->iteration  }}</th>
                    <td>{{ $var -> categori }}</td>
                    <td>{{ $var -> price }}</td>
                    <td>{{ $var -> subdesc }}</td>
                    <td>{{ $var -> star }}</td>
                    <td>
                            @foreach ($var -> privilage as $item)
                                {{ $item -> privilage }} <br>
                            @endforeach

                    </td>

                    <td>
                        <div class="d-flex justify-content-end">
                                <form action="{{ route('admin.edit-pricing')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name='id' value="{{ $var -> id }}">
                                <button type="submit" class="mx-3 btn btn-warning"><i class="bi bi-pencil-square"></i></button>
                            </form>
                            <form action="{{ route('admin.destroy-pricing')}}" method="POST">
                                @csrf
                                <input type="hidden" name='id' value="{{ $var -> id }}">
                                <button  type="submit" class="me-3 btn btn-danger"><i class="bi bi-trash"></i></button>
                            </form>

                            </td>
                        </div>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <!-- End Default Table Example -->
    </div>
</div>
@endsection
