@extends('layout.admin')
@section('content')

<div class="card">
    <form class="row g-3" action="{{$pricing ?  route('admin.update-pricing') : route('admin.store-pricing')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @if ($pricing)
            <input type="text" name="id" value="{{ $pricing -> id }}" hidden>
        @endif
    <div class="card-body">



        <!-- Floating Labels Form -->
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="categori" value="{{ $pricing ? $pricing -> categori : ''}}" placeholder="Your Name">
                    <label for="floatingName">Categori</label>
                </div>
            </div>
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="number" class="form-control" id="floatingName" name="price" value="{{ $pricing ? $pricing -> price : ''}}" placeholder="Your Name">
                    <label for="floatingName">Price</label>
                </div>
            </div>
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="number" class="form-control" id="floatingName" name="star" value="{{ $pricing ? $pricing -> star : ''}}" placeholder="Your Name">
                    <label for="floatingName">Star</label>
                </div>
            </div>
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="subdesc" value="{{ $pricing ? $pricing -> subdesc : ''}}" placeholder="Your Name">
                    <label for="floatingName">Subdesc</label>
                </div>
            </div>



           @if ($pricing)

           <div class="col-md-12 mb-3 mt-3" id="parentSubdesc">

            @foreach ($pricing -> privilage as $item )
            <div class="d-flex justify-content-end my-2" id="Subdesc">
                <div class="form-floating col-md-10">
                    <input type="text" class="form-control" id="floatingName" name="privilage[]" value="{{ $item -> privilage }}" placeholder="Privilage">
                    <label for="floatingName">Privilage</label>
                </div>
                <button type="button" id="plus" class="btn btn-success mx-3 my-2"><i class="bi bi-plus-square"></i></button>
                <button type="button" id="minus" class="btn btn-danger mx-3 my-2"><i class="bi bi-patch-minus"></i></button>
                </div>
            @endforeach

        </div>

           @else

           <div class="col-md-12 mb-3 mt-3" id="parentSubdesc">
            <div class="d-flex justify-content-end my-2" id="Subdesc">
            <div class="form-floating col-md-10">
                <input type="text" class="form-control" id="floatingName" name="privilage[]" value="{{ $pricing ? $pricing -> title : ''}}" placeholder="Your Name">
                <label for="floatingName">Privilage</label>
            </div>
            <button type="button" id="plus" class="btn btn-success mx-3 my-2"><i class="bi bi-plus-square"></i></button>
            <button type="button" id="minus" class="btn btn-danger mx-3 my-2"><i class="bi bi-patch-minus"></i></button>
            </div>
        </div>

           @endif






    <div class="text-center mt-5">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
    </div>
    </form><!-- End floating Labels Form -->

    <!-- End Quill Editor Default -->

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
      $("#plus").click(function(){
        $("#Subdesc").clone().appendTo("#parentSubdesc");
      });
    });

    </script>

<script>
    $(document).ready(function(){
      $("#minus").click(function(){
        $("#parentSubdesc").remove('#Subdecs');
      });
    });
</script>

@endsection
