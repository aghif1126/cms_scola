@extends('layout.admin')
@section('content')

<div class="card">
    <form class="row g-3" action="{{$quote ?  route('admin.update-quote') : route('admin.store-quote')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @if ($quote)
            <input type="text" name="id" value="{{ $quote -> id }}" hidden>
        @endif
    <div class="card-body">



        <!-- Floating Labels Form -->
            <div class="col-md-12 mb-3 mt-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="title" value="{{ $quote ? $quote -> title : ''}}" placeholder="Your Name">
                    <label for="floatingName">Add Quote</label>
                </div>
            </div>






    <div class="text-center mt-5">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
    </div>
    </form><!-- End floating Labels Form -->

    <!-- End Quill Editor Default -->

</div>




@endsection
